import { LitElement, html } from 'lit-element';

export class EjercicioCinco extends LitElement {
  static get properties() {
    return {
      clicks: { type: Number },
    };
  }


  constructor() {
    super();
    this.clicks = 0;
  }

  incrementarClick(){
    this.clicks++;
  }

  render() {
    return html`
      <h1>MyCounter</h1>
      <p>Clicks: ${this.clicks}</p>
      <button @click="${this.incrementarClick}">Haz click</button>
    `;
  }
}

customElements.define('ejercicio-cinco', EjercicioCinco);