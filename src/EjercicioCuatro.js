import { LitElement, html } from 'lit-element';

export class EjercicioCuatro extends LitElement {
  static get properties() {
    return {
      myProperty: {
        type: Number,
        hasChanged(newValue, oldValue){
          if(newValue % 2 === 0){
            return newValue * 2;
          }else{
            return newValue;
          }

        }
      }
    };
  }


  constructor() {
    super();
  }

  render() {
    return html`
      <h1>MyComponent</h1>
      <span>${this.myProperty}</span>
    `;
  }
}

customElements.define('ejercicio-cuatro', EjercicioCuatro);