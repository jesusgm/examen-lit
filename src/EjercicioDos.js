import { LitElement, html } from 'lit-element';

export class EjercicioDos extends LitElement {
  static get properties() {
    return {
      names: { type: Array }
    };
  }

  render() {
    return html`
      <ul>
      ${this.names.map(n => html`
        <li>${n}</li>
      `)}
      </ul>
    `;
  }
}

customElements.define('ejercicio-dos', EjercicioDos);