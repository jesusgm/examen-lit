import { LitElement, html } from 'lit-element';

export class EjercicioOcho extends LitElement {
  static get properties() {
    return {
      clicks: {type: Number}
    };
  }


  constructor() {
    super();
    this.clicks = 0;
  }

  counter(){
      this.clicks++;
  }

  render() {
    return html`
        <button @click="${this.counter()}">Clicks</button>
        <p>click counter: ${this.clicks}</p>
      
    `;
  }
}

customElements.define('ejercicio-ocho', EjercicioOcho);