import { LitElement, html } from 'lit-element';

export class EjercicioSeis extends LitElement {
  static get properties() {
    return {
      fruits: { type: Array },
    };
  }


  constructor() {
    super();
    this.fruits=["uva","pera","arandano","manzana","naranja"];
  }

  sortList(){
    //this.fruits.sort();
    //let temporal = this.fruits(sort); this.fruits = temporal;
    //this.fruits = [...this.fruits.sort()];
    //this.fruits.sort(); this.performUpdate();
    //console.log(this.fruits);
  }

  render() {
    return html`
      <ul>
        ${this.fruits.map(f => html`
          <li>${f}</li>
        `)}
      </ul>
      <button @click="${this.sortList}">Ordenar</button>
    `;
  }
}

customElements.define('ejercicio-seis', EjercicioSeis);