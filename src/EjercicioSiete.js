import { LitElement, html } from 'lit-element';

export class EjercicioSiete extends LitElement {
  static get properties() {
    return {
      name: {
        type: String,
        value: 'Juan'
      }
    };
  }


  constructor() {
    super();
  }

  render() {
    return html`
      <p>Hola ${this.name}</p>
    `;
  }
}

customElements.define('ejercicio-siete', EjercicioSiete);