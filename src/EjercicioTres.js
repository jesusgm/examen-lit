import { LitElement, html } from 'lit-element';

export class EjercicioTres extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      age: { type: Number}
    };
  }

  render() {
    return html`

      ${this.name} ${!(this.age >= 18) ? 
        html`es mayor de edad`
        :
        html`es menor de edad`
      }
    `;
  }
}

customElements.define('ejercicio-tres', EjercicioTres);