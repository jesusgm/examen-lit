import { LitElement, html } from 'lit-element';

export class EjercicioUno extends LitElement {
  static get properties() {
    return {
        hiden: {type: Boolean},
        state: {type: Number}
    };
  }


  constructor() {
    super();
    this.hiden = false;
    this.state = 0;
  }

  handleClick(){
    this.hiden = !this.hiden;
    this.state += 1 + this.hiden;
  }
  
  handleClock() {
    this.hiden = !this.hiden;
    this.state += 2 + this.hiden;
  } 

  render() {
    return html`
        ${this.hiden ? 
            html`<button @click="${this.handleClick}">Contador</button>`
            :
            html`<button @click="${this.handleClock}">Contador</button>`
        }
    `;
  }
}

customElements.define('ejercicio-uno', EjercicioUno);