import { LitElement, html } from 'lit-element';

import './EjercicioUno';
import './EjercicioDos';
import './EjercicioTres';
import './EjercicioCuatro';
import './EjercicioCinco';
import './EjercicioSeis';
import './EjercicioSiete';
import './EjercicioOcho';

export class TestApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }


  constructor() {
    super();
  }

  render() {
    return html`
      <!--
      <ejercicio-uno></ejercicio-uno>
      <ejercicio-dos names = '["Jose","Maria","Pedro","Ana"]'></ejercicio-dos>
      <ejercicio-tres name="Juan" age="19"></ejercicio-tres>
      <ejercicio-cuatro></ejercicio-cuatro>
      <ejercicio-cinco></ejercicio-cinco>
      <ejercicio-seis></ejercicio-seis>
      <ejercicio-siete></ejercicio-siete>
      <ejercicio-ocho></ejercicio-ocho>
      -->
    `;
  }
}

customElements.define('test-app', TestApp);